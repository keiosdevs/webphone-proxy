#!/bin/bash

function not_debian8_GTFO {
	echo -e "\e[31mThis software can be only installed on Debian 8 linux system. If you want to override this, edit this script."
#	exit 1
}

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [ -f /etc/debian_version ]; then
	debian_version=$(cat /etc/debian_version)
	if [[ ! $debian_version =~ ^8.*$ ]]; then
		not_debian8_GTFO
	fi
else
	not_debian8_GTFO
fi

if [ -z ${1} ] || [ -z ${2} ] || [ -z ${3} ] || [ -z ${4} ] ; then 
	echo "Usage: ./install.sh <local_ip> <local_domain> <target_ip> <target_port>"
	exit
fi

local_ip=$1
local_domain=$2
target_ip=$3
target_port=$4

echo "Generating TURN server password..."
turn_password=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)

find etc -type f -print0 | xargs -0 sed -i "s/XXXXX-XXXXX/$local_ip/g"
find etc -type f -print0 | xargs -0 sed -i "s/XXXX-XXXX/$local_domain/g"
find etc -type f -print0 | xargs -0 sed -i "s/XXX-XXX/$local_domain/g"
find etc -type f -print0 | xargs -0 sed -i "s/XXX.XXX.XXX.XXX/$target_ip/g"
find etc -type f -print0 | xargs -0 sed -i "s/XXXX/$target_port/g"
find etc -type f -print0 | xargs -0 sed -i "s/XXXX-TURN-PASS/$turn_password/g"

echo "Installing dependencies..."
apt-get -y install dpkg-dev debhelper iptables-dev libcurl4-openssl-dev libglib2.0-dev \
  libhiredis-dev libpcre3-dev libssl-dev markdown zlib1g-dev libxmlrpc-core-c3-dev dkms linux-headers-`uname -r` \
  kamailio kamailio-websocket-modules kamailio-tls-modules turnserver libevent-dev libpcap-dev \
  libmysqlclient-dev libavformat-dev libavfilter-dev libavutil-dev libavresample-dev libjson-glib-dev

echo -e "\e[32mBuilding RTPEngine...\e[39m"
echo -e "\e[32mCloning...\e[39m"
git clone https://github.com/viamage/rtpengine.git
cd rtpengine
git checkout mr5.5.3
echo -e "\e[32mConfiguring...\e[39m"
./debian/flavors/no_ngcp
echo -e "\e[32mCompiling & packaging...\e[39m"
dpkg-buildpackage
cd ..
mkdir toinstall
mv ngcp-rtpengine-daemon_* ngcp-rtpengine-iptables_* ngcp-rtpengine-kernel-dkms_* toinstall
echo -e "\e[32mInstalling...\e[39m"
dpkg -iR toinstall
echo -e "\e[32mCleaning up...\e[39m"
rm -fr toinstall rtpengine ngcp*

echo -e "Copying configs..."
cp -f etc/default/* /etc/default/
cp -f etc/kamailio/* /etc/kamailio/
cp -f etc/turn* /etc/

echo -e "Restarting services..."
/etc/init.d/ngcp-rtpengine-daemon restart
/etc/init.d/kamailio restart
/etc/init.d/turnserver restart

echo -e "Configuring LetsEncrypt..."
echo 'deb http://ftp.debian.org/debian jessie-backports main' | sudo tee /etc/apt/sources.list.d/backports.list
apt-get update
apt-get install nginx certbot -t jessie-backports
mkdir /var/www/html
yes|cp etc/nginx/sites-available/default /etc/nginx/sites-available/default
sed -i "s/SERVER_NAME/$local_domain/g" /etc/nginx/sites-available/default
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/default /etc/nginx/sites-enabled/default
systemctl restart nginx
certbot certonly --webroot -w /var/www/html/ -d $local_domain
sed -i "s/SERVER_NAME/$local_domain/g" usr/bin/copy_cert
cp usr/bin/copy_cert /usr/bin/copy_cert
chmod +x /usr/bin/copy_cert
/usr/bin/copy_cert
echo -e "Installation finished, please add following commands to your cron:"
echo -e "0 3 1 * * certbot renew"
echo -e "5 3 1 * * /usr/bin/copy_cert"
echo -e "Now run:"
echo -e "systemctl restart kamailio"
echo -e "Your TURN server username is '\e[32mkeios-webphone\e[39m' and password is '\e[32m$turn_password\e[39m'. Use those values in client configuration."
