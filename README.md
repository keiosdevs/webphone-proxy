#KEIOS SIP+RTP WebSocket+WebRTC Proxy
####Make Web-based SIP phones less shitty again. 

"Ever heard of ICE-based NAT traversal for SRTP packets via WebRTC, berk?" - unknown VOIP Factol

You will need git, obviously. 

You will also need:

- target voip switch ip and tcp port (_no tls for nao, but it's trivial to add it (t_relay_to_tcp->t_relay_to_tls is a protip)_)
- public ip of your **DESIGNATED PROXY SERVER**
- domain dedicated to SIP WebSockets server (ie wssip.your.domain, should point to **DESIGNATED PROXY SERVER**)
- SSL certificate chain and private key for this domain 

Clone repo, then:

```
chmod +x install.sh
./install.sh DESIGNATED_PROXY_SERVER_IP DESIGNATED_PROXY_DOMAIN target_voip_switch_ip target_voip_switch_port
```

You will be given TURN server username (keios-webphone) and _generated password_. Write password down, feed it to client component.
Copy SSL certificate chain and private key to `/etc/kamailio` as `fullchain.pem` and `privkey.pem`. Restart kamailio.
```
systemctl restart kamailio
```

####Remember to pray to the [Machine Spirit](https://machinespir.it). Burn some silica incense - this helps, really.